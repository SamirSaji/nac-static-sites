import React from "react";
import "./screens.css";
export const Links = () => {
  const _links = [
    { title: "Home Page", url: "/" },
    { title: "Collection Page", url: "/collectionpage" },
    { title: "Savings Page", url: "/savingscheme" },
    { title: "Experience Page", url: "/experiences" },
    { title: "Advertising Page", url: "/advertising" },
    { title: "Temple Work Page", url: "/temple-work" },
    { title: "News Room Page", url: "/newsroom" },
    { title: "About Us", url: "/aboutus" },
    { title: "Terms", url: "/terms" },
    { title: "Return", url: "/return" },
    { title: "Delivery", url: "/delivery" },
    { title: "Privacy", url: "/privacy" },
    { title: "Contact Page", url: "/contactus" },
    { title: "Education", url: "/education" },
    { title: "Site Map", url: "/sitemap" },
    { title: "Faq", url: "/faq" },
    { title: "Store Locator", url: "/store" },
    { title: "Store Detail", url: "/loc/:id" },
    { title: "Careers", url: "/careers" },
    { title: "Digital Marketing", url: "/careers/1" },
    { title: "Store Location Details", url: "/loc" },
    { title: "Diamonds", url: "/solitaires-search" },
    { title: "Collection Home Page", url: "/collectionhome" },
    { title: "rudramadevi Blog", url: "/rudramadeviBlog" },
    { title: "Solitaires", url: "/solitaires" },
    { title: "links", url: "/allmenus" },
  ];
  return (
    <>
      <h1 className="h1class">NAC Static Pages Menu Lists</h1>
      <div className="h1class">Total No Of Static Pages build is {_links.length}</div>
      <aside>
        <ul>
          {_links.map((val) => {
            return (
              <li>
                <div class="icons">
                  <i class="fa fa-align-justify"></i>
                </div>
                <a href={val.url} target="_blank" rel="noopener noreferrer">
                  <div class="linktitle">{val.title}</div>
                </a>
              </li>
            );
          })}
        </ul>
      </aside>
    </>
  );
};

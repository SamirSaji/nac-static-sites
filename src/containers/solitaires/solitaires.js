import React from "react";
import { Grid, Typography } from "@material-ui/core";
import Slideshow from "../../components/carousal/carousal";
import Header from "components/header/header";
import { SolitairesData } from "../../mappers/dummydata/solitairesData";
import styles from "../solitaires/solitairestyle";
export default function Solitaires(props) {
  const classes = styles();
  const next = () => {
    slider.current.slickNext();
  };
  const previous = () => {
    slider.current.slickPrev();
  };
  const slider = React.createRef();

  return (
    <Grid container>
      <Grid item>{/* <Header /> */}</Grid>

      <Grid
        item
        xs={12}
        sm={12}
        md={12}
        lg={12}
        xl={12}
        className={classes.bannerImg}
      >
        {/* <Hidden smDown> */}
        {SolitairesData.setting.arrowsImg && (
          <Grid container>
            <Grid item onClick={previous} className={classes.preButton}></Grid>
            <Grid item onClick={next} className={classes.nextButton}></Grid>
          </Grid>
        )}
        {/* </Hidden> */}
        <Slideshow dataCarousel={SolitairesData.setting} sliderRef={slider}>
          {SolitairesData.carouselData.map((val, index) => (
            <Grid container key={index} className={classes.headContent}>
              <Typography className={classes.imageContent}>{SolitairesData.imageContent}</Typography>
              <img key={index} src={val.img} className={classes.mainCarosel} />
            </Grid>
          ))}
        </Slideshow>
      </Grid>
      <Grid container className={classes.contentPart}>
        <Grid item xs={10} sm={10} md={7} lg={7} xl={7}>
          <Grid>
            <Typography className={classes.heading}>
              {SolitairesData.heading}
            </Typography>
          </Grid>
          <Grid>
            <Typography className={classes.bodyContent}>
              {SolitairesData.bodyContent}
            </Typography>
          </Grid>
          <Grid className={classes.hrGrid} xs={3} sm={3} md={2} lg={2} xl={2}>
            <hr className={classes.hrTag}></hr>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

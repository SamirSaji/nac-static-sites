import { makeStyles } from "@material-ui/core/styles";
const styles = makeStyles(theme => ({
    numEffect:{
        backgroundColor: "#000",
    color: "#fff",
    borderRadius: "50%",
    padding: "4px 11px",
    textAlign: "center",
    marginRight: "10px",
    display: "inline-block",
    float:"left",
    },
    mainContainer:{
      boxShadow:  "1px solid #d8d8d8",
      paddingBottom:"50px",
    },
    containers:{
        fontFamily: "Libre Baskerville",
        fontSize: "20px",
        textAlign: "center",
        padding: "20px 0",
        color: "#000",
        display: "flex",
        justifyContent: "center",
        padding:"15px 0px",
    
    },
    containersBor:{
        fontFamily: "Libre Baskerville",
        fontSize: "20px",
        textAlign: "center",
        padding: "20px 0",
        color: "#000",
        display: "flex",
        justifyContent: "center",
        padding:"15px 0px",
        border: "1px solid #d8d8d8",
        borderLeft:"none",
    
    },
    text:{
        fontFamily: "Libre Baskerville",
        fontSize: "20px",
        textAlign: "center",
        color: "#000",
        float:"left",

    },
    containers2:{
        width: "100%",
    float: "left",
    paddingTop: "20px",
    },
    containers2Item:{
        position: "relative",
    margin: "0",
    padding: "0",
    float: "left",
    width: "100%",
    
    },
    image1:{
        backgroundImage: "url(https://www.nacjewellers.com/rstatic/dist/28785c70c75542c44e626a2ab0f0725a.png)",
    backgroundRepeat: "no-repeat",
    width: "71px",
    height: "57px",
    display: "inline-block",
    margin: "0 20px",
    listStyle: "none",
    padding: "0",
    marginBottom: "25px",
    backgroundPosition: "13px 0",
    '&:hover':{
        backgroundPosition: "13px -57px",
        cursor:"pointer",
    }
    },
    image2:{
        backgroundImage: "url(https://www.nacjewellers.com/rstatic/dist/28785c70c75542c44e626a2ab0f0725a.png)",
    backgroundRepeat: "no-repeat",
    width: "71px",
    height: "57px",
    display: "inline-block",
    margin: "0 20px",
    listStyle: "none",
    padding: "0",
    marginBottom: "25px",
    backgroundPosition: "-73px 0",
    '&:hover':{
        backgroundPosition: "-73px -57px",
        cursor:"pointer",
    }
    },
    image3:{
        backgroundImage: "url(https://www.nacjewellers.com/rstatic/dist/28785c70c75542c44e626a2ab0f0725a.png)",
    backgroundRepeat: "no-repeat",
    width: "71px",
    height: "57px",
    display: "inline-block",
    margin: "0 20px",
    listStyle: "none",
    padding: "0",
    marginBottom: "25px",
    backgroundPosition: "-158px 0",
    '&:hover':{
        backgroundPosition: "-158px -57px",
        cursor:"pointer",
    }
    },
    image4:{
        backgroundImage: "url(https://www.nacjewellers.com/rstatic/dist/28785c70c75542c44e626a2ab0f0725a.png)",
    backgroundRepeat: "no-repeat",
    width: "71px",
    height: "57px",
    display: "inline-block",
    margin: "0 20px",
    listStyle: "none",
    padding: "0",
    marginBottom: "25px",
    backgroundPosition: "-239px 0",
    '&:hover':{
        backgroundPosition: "-239px -57px",
        cursor:"pointer",
    }
    },
    image5:{
        backgroundImage: "url(https://www.nacjewellers.com/rstatic/dist/28785c70c75542c44e626a2ab0f0725a.png)",
    backgroundRepeat: "no-repeat",
    width: "71px",
    height: "57px",
    display: "inline-block",
    margin: "0 20px",
    listStyle: "none",
    padding: "0",
    marginBottom: "25px",
    backgroundPosition: "-615px 0",
    '&:hover':{
        backgroundPosition: "-615px -57px",
        cursor:"pointer",
    }
    },
    image6:{
        backgroundImage: "url(https://www.nacjewellers.com/rstatic/dist/28785c70c75542c44e626a2ab0f0725a.png)",
    backgroundRepeat: "no-repeat",
    width: "71px",
    height: "57px",
    display: "inline-block",
    margin: "0 20px",
    listStyle: "none",
    padding: "0",
    marginBottom: "25px",
    backgroundPosition: "-322px 0",
    '&:hover':{
        backgroundPosition: "-322px -57px",
        cursor:"pointer",
    }
    },
    image7:{
        backgroundImage: "url(https://www.nacjewellers.com/rstatic/dist/28785c70c75542c44e626a2ab0f0725a.png)",
    backgroundRepeat: "no-repeat",
    width: "71px",
    height: "57px",
    display: "inline-block",
    margin: "0 20px",
    listStyle: "none",
    padding: "0",
    marginBottom: "25px",
    backgroundPosition: "-400px 0",
    '&:hover':{
        backgroundPosition: "-400px -57px",
        cursor:"pointer",
    }
    },
    image8:{
        backgroundImage: "url(https://www.nacjewellers.com/rstatic/dist/28785c70c75542c44e626a2ab0f0725a.png)",
    backgroundRepeat: "no-repeat",
    width: "71px",
    height: "57px",
    display: "inline-block",
    margin: "0 20px",
    listStyle: "none",
    padding: "0",
    marginBottom: "25px",
    backgroundPosition: "-536px 0",
    '&:hover':{
        backgroundPosition: "-536px -57px",
        cursor:"pointer",
    }
    },
    image9:{
        backgroundImage: "url(https://www.nacjewellers.com/rstatic/dist/28785c70c75542c44e626a2ab0f0725a.png)",
    backgroundRepeat: "no-repeat",
    width: "71px",
    height: "57px",
    display: "inline-block",
    margin: "0 20px",
    listStyle: "none",
    padding: "0",
    marginBottom: "25px",
    backgroundPosition: "-465px 0",
    '&:hover':{
        backgroundPosition: "-465px -57px",
        cursor:"pointer",
    }
    },
    name:{
        position: "relative",
    top: "81px",
    left: "6px",
    display: "block",
    textAlign: "center",
    textDecoration:"none",
    color: "#5d5d5d",
    fontSize: "14px",
    },
    slider_wrapper:
    {
        paddingLeft: "12.5%",
        paddingRight: "12.5%",
        position: "relative",
    },
    slider_range:
        {
            backgroundColor: "#3A3A3A",
            height: "8px",
            marginTop: "20px",
        },
        slider_handle:
        {
            backgroundImage: "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAbCAMAAABGHy+tAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAE5QTFRFQKndVMHtn9TuRa/hT7vp3/P8iMnqX8Xuv+j48/r92+/59Pv+TK7ft9/yZLnjn9z1w+T0RK7glNj01O/6itTzz+n2jdDudM3wWr/q////KtnB+gAAABp0Uk5T/////////////////////////////////wAUIgDaAAAAaklEQVR42mKQxAAMRAqJMqECoBATAyoYWkICEJYgCwRIgLzNBhZiZgQDdlaQECcfkhAHJHC4eOBC/LDw4oUJiSCCkA0iJM6NEAIaxww2GimguXiAQsKoYc/LDLIMNTrEhDBiiJvEeAQIMAD7whbSySaxwQAAAABJRU5ErkJggg==)",
            backgroundPosition: "left top",
            backgroundRepeat: "no-repeat",
            cursor: "pointer",
            height: "20px",
            position: "absolute",
            top: "-20px",
            width: "20px",
        },
        algin:{
            padding:"5px 20px"
        },
        

}))
export default styles;
export const routes = {
  HomePage: "/",
  CollectionPage: "/collectionpage",
  SavingsPage: "/savingscheme",
  ExperiencePage: "/experiences",
  AdvertisingPage : '/advertising',
  TempleWorkPage : '/temple-work',
  NewsRoomPage : '/newsroom',
  AboutUs: "/aboutus",
  Terms:"/terms",
  Return:"/return",
  Delivery:"/delivery",
  Privacy:"/privacy",
  ContactPage  :'/contactus',
  Education:"/education",
  SiteMap:"/sitemap",
  Faq:"/faq",
  StoreLocator:"/store",
  StoreDetail:"/loc/:id",
  Careers:"/careers",
  DigitalMarketing : '/careers/1',
  StoreLocationDetails:'/loc',
  Diamonds:'/solitaires-search',
  CollectionHomePage : '/collectionhome',
  rudramadeviBlog:'/rudramadeviBlog',
  Solitaires : "/solitaires",

  // Temp
  links:'/allmenus',
};

export default routes;

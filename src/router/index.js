import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

// import { GlobalContext } from 'context'

import routes from "router/routes";
import { withRouter } from "react-router";
import { createBrowserHistory } from "history";
import { Home } from "screens";
import CollectionPage from "containers/collection/collectionPage";
import SavingsPage from "containers/savings/savings";
import AboutUs from "containers/aboutus/aboutus";
import AdvertisingPage from "containers/advertising/advertising";
import TempleWorkPage from "containers/templework/templework";
import ContactPage from "containers/contactus/contact";
import NewsRoomPage from "containers/newsroom/newsroom";
import Locator from "containers/storelocator/index";
import StoreLocationDetails from "components/StoreLocatorDetail";
import DigitalMarketing from "containers/digitalmarketing/digitalmarketing";
import Diamonds from "containers/diamonds/looseDiamonds";
import CollectionHomePage from "containers/collectionhomepage/collectionHomePage";
import careers from "containers/careers/careers";
import rudramadeviBlog from "containers/collection/rudramadeviBlog";
import Solitaires from "../containers/solitaires/solitaires";
import { Links } from "../screens/links";
const browserHistory = createBrowserHistory();

browserHistory.listen((location, action) => {
  window.scrollTo(0, 0);
});
export const RouterApp = (props) => {
  // const { Globalctx } = React.useContext(GlobalContext)

  return (
    <Switch history={browserHistory}>
      <Route key="HomePage" component={Home} exact path={routes.HomePage} />
      <Route
        key="CollectionPage"
        component={CollectionPage}
        exact
        path={routes.CollectionPage}
      />
      <Route
        key="SavingsPage"
        component={SavingsPage}
        exact
        path={routes.SavingsPage}
      />
      <Route
        key="ExperiencePage"
        component={SavingsPage}
        exact
        path={routes.ExperiencePage}
      />
      <Route
        key="AdvertisingPage"
        component={AdvertisingPage}
        exact
        path={routes.AdvertisingPage}
      />
      <Route
        key="TempleWorkPage"
        component={TempleWorkPage}
        exact
        path={routes.TempleWorkPage}
      />
      <Route
        key="NewsRoomPage"
        component={NewsRoomPage}
        exact
        path={routes.NewsRoomPage}
      />
      <Route
        key="DigitalMarketing"
        component={DigitalMarketing}
        exact
        path={routes.DigitalMarketing}
      />
      <Route
        key="CollectionHomePage"
        component={CollectionHomePage}
        exact
        path={routes.CollectionHomePage}
      />

      <Route key="AboutUs" component={AboutUs} exact path={routes.AboutUs} />
      <Route key="Terms" component={AboutUs} exact path={routes.Terms} />
      <Route key="Return" component={AboutUs} exact path={routes.Return} />
      <Route key="Delivery" component={AboutUs} exact path={routes.Delivery} />
      <Route key="Privacy" component={AboutUs} exact path={routes.Privacy} />
      <Route key="careers" component={careers} exact path={routes.Careers} />
      <Route
        key="ContactPage"
        component={ContactPage}
        exact
        path={routes.ContactPage}
      />
      <Route
        key="StoreLocator"
        component={Locator}
        exact
        path={routes.StoreLocator}
      />
      <Route
        key="StoreLocationDetails"
        component={StoreLocationDetails}
        exact
        path={routes.StoreDetail}
      />
      <Route key="Diamonds" component={Diamonds} exact path={routes.Diamonds} />
      <Route
        key="rudramadeviBlog"
        component={rudramadeviBlog}
        exact
        path={routes.rudramadeviBlog}
      />

      <Route key="careers" component={careers} exact path={routes.Careers} />
      <Route
        key="ContactPage"
        component={ContactPage}
        exact
        path={routes.ContactPage}
      />
      <Route
        key="StoreLocator"
        component={Locator}
        exact
        path={routes.StoreLocator}
      />
      <Route
        key="StoreLocationDetails"
        component={StoreLocationDetails}
        exact
        path={routes.StoreDetail}
      />

      <Route
        key="Education"
        component={AboutUs}
        exact
        path={routes.Education}
      />
      <Route key="SiteMap" component={AboutUs} exact path={routes.SiteMap} />
      <Route key="Faq" component={AboutUs} exact path={routes.Faq} />
      <Route key="Menu" component={Links} exact path={routes.links} />

      <Route
        key="Solitaires"
        component={Solitaires}
        exact
        path={routes.Solitaires}
      />
    </Switch>
  );
};
export default withRouter(RouterApp);

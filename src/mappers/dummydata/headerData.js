// mobile views menu
export const mainlist = [

    {
        name: "Jewellery",
        url: "/jewellery",
    },
    {
        name: "Silver",
        url: "/stylorisilver",
    },
    {
        name: "GoldCoins",
        url: "/goldcoins",
    },
    {
        name: "Collections",
        url: "/collections",
    },
    {
        name: "Ready to ship",
        url: "/jewellery-shipping+in+1+day",
    }, {
        name: "Stories",
        url: "/stories",
    },
];

/// mobile views submenu
export const subheader = {
    "Earrings": {
        header: 'Earrings',
        name: [
            {
                name: 'Studs',
                url: "/studs-earrings-jewellery"
            }, {
                name: 'Drop',
                url: "/drops-earrings-jewellery"
            }, {
                name: 'Ear Cuffs',
                url: "/ear+cuffs-earrings-jewellery"
            }, {
                name: 'Huggies',
                url: "/huggies-earrings-jewellery"
            }, {
                name: 'Jhumkas',
                url: "/jhumkas+online-earrings-jewellery"
            }, {
                name: 'Ear Jacket',
                url: "/ear+jacket-earrings-jewellery"
            }
        ]
    },
    "Pendants": {
        header: 'Pendants',
        name: [
            {
                name: 'Classic',
                url: "/classic-pendants-jewellery"
            }, {
                name: 'Casual',
                url: "/casual-pendants-jewellery"
            }, {
                name: 'Fashion',
                url: "/fashion-pendants-jewellery"
            }, {
                name: 'Religious',
                url: "/religious-pendants-jewellery"
            }, {
                name: 'Tanmaniya',
                url: "/tanmaniya-pendants-jewellery"
            },
        ]
    },
    "Rings": {
        header: 'Rings',
        name: [
            {
                name: 'Classic',
                url: "/classic-rings-jewellery"
            }, {
                name: 'Casual',
                url: "/casual-rings-jewellery"
            }, {
                name: 'Cocktail',
                url: "/cocktail-rings-jewellery"
            }, {
                name: 'Engagement',
                url: "/rings-jewellery-for+engagement"
            }, {
                name: 'Fashion',
                url: "/fashion-rings-jewellery"
            }, {
                name: 'Men\'s Ring',
                url: "/rings-jewellery-for+men"
            },
        ]
    },
    "Nose Pins": {
        header: 'Nose Pins',
        name: [
            {
                name: 'Classic',
                url: "/classic-nose+pin+online-jewellery"
            }, {
                name: 'Fashion',
                url: "/fashion-nose+pin+online-jewellery"
            }, {
                name: 'Stud Nose Pin',
                url: "/nose+studs+online-jewellery"
            },
        ]
    },
    'Bangles & Bracelets': {
        header: 'Bangles Bracelets',
        name: [
            {
                name: 'Bangles',
                url: "/bangles-jewellery"
            }, {
                name: 'Bracelets',
                url: "/bracelets-jewellery"
            }, {
                name: 'Oval Bracelets',
                url: "/oval+bracelets-jewellery"
            },
        ]
    },
    'By Price': {
        header: 'By Price',
        name: [
            {
                name: 'Under Rs 5000',
                url: "/jewellery?startprice=1000&endprice=5000"
            }, {
                name: 'Rs 5000 - Rs 10000',
                url: "/jewellery?startprice=5000&endprice=10000"
            }, {
                name: 'Rs 10000 - Rs 20000',
                url: "/jewellery?startprice=10000&endprice=20000"
            }, {
                name: 'Above Rs 20000',
                url: "/jewellery?startprice=10000&endprice=20000"
            },
        ]
    },
    'By Collections': {
        header: 'By Collections',
        name: [
            {
                name: 'Carve',
                url: "/jewellery-from+the+carve+collection?sort=latest"
            }, {
                name: 'Blush',
                url: "/jewellery-blush"
            }, {
                name: 'Molecute',
                url: "/jewellery-molecute+collection"
            }, {
                name: 'Gemstone',
                url: "/gemstone-jewellery?sort=latest"
            }, {
                name: 'Mistletoe',
                url: "/jewellery-from+mistletoe+collection"
            }, {
                name: 'The Renaissance',
                url: "/jewellery-from+the+renaissance+collection"
            }, {
                name: 'Monsoon',
                url: "/jewellery-from+monsoon+collection"
            }, {
                name: 'Summer',
                url: "/jewellery-from+the+summer+collection"
            },
        ]
    },
    'My Material': {
        header: 'By Material',
        name: [
            {
                name: 'Diamond',
                url: "/diamond-jewellery"
            }, {
                name: 'Gemstone',
                url: "/gemstone-jewellery"
            }, {
                name: 'Solitaire',
                url: "/solitaire-jewellery"
            },
        ]
    },
    'Silver Jewellery': {
        header: 'Silver Jewellery',
        name: [
            {
                name: 'Earrings',
                url: "stylori.net/silver-earrings-jewellery"
            }, {
                name: 'Rings',
                url: "stylori.net/silver-rings-jewellery"
            }, {
                name: 'Pendants',
                url: "stylori.net/silver-pendants-jewellery"
            },
        ]
    },
    "By design": {
        header: 'By design',
        name: [
            {
                name: 'Plain',
                url: "/plain-goldcoins"
            }, {
                name: 'Lakshmi',
                url: "/lakshmi-goldcoins"
            }, {
                name: 'Jesus',
                url: "/jesus-goldcoins"
            }, {
                name: 'Ganesh',
                url: "/ganesha-goldcoins"
            }, {
                name: 'Balaji ',
                url: "/balaji-goldcoins"
            },
        ]
    },
    "By Weight": {
        header: 'By Weight',
        name: [
            {
                name: '1 Gram',
                url: "/1gm-goldcoins"
            }, {
                name: '4 Grams',
                url: "/4gms-goldcoins"
            }, {
                name: '8 Grams',
                url: "/8gms-goldcoins"
            }, {
                name: '10 Grams',
                url: "/10gms-goldcoins"
            }, {
                name: '20 Grams',
                url: "/20gms-goldcoins"
            }, {
                name: '50 Grams',
                url: "/50gms-goldcoins"
            },
        ]
    },
    'By Purity': {
        header: 'By Purity',
        name: [
            {
                name: '24 KT',
                url: "/24kt-goldcoins"
            }, {
                name: '22 KT',
                url: "/22kt-goldcoins"
            },
        ]
    },
    'By Collections': {
        header: 'By Collections',
        name: [
            {
                name: 'Halo',
                url: "/jewellery-from+halo+collection"
            }, {
                name: 'Daisy Days',
                url: "/jewellery-from+daisy+days+collection"
            }, {
                name: 'Monsoon',
                url: "/jewellery-from+monsoon+collection"
            }, {
                name: 'Mango',
                url: "/jewellery-from+the+summer+collection"
            }, {
                name: 'Blush',
                url: "/jewellery-blush"
            }, {
                name: 'Molecute',
                url: "/jewellery-molecute+collection"
            }, {
                name: 'Gemstone',
                url: "/gemstone-jewellery?sort=latest"
            }, {
                name: 'Butterfly',
                url: "/jewellery-butterfly?sort=latest"
            },
        ]
    },
    'By Theme': {
        header: 'By Theme',
        name: [
            {
                name: 'Carve',
                url: "/jewellery-from+the+carve+collection?sort=latest"
            }, {
                name: 'Cluster',
                url: "/cluster-jewellery"
            }, {
                name: 'Droplets',
                url: "/droplets-jewellery"
            }, {
                name: 'Waves',
                url: "/waves-jewellery"
            }, {
                name: 'Floral',
                url: "/waves-jewellery"
            }, {
                name: 'Hearts',
                url: "/floral-jewellery"
            }, {
                name: 'Tiara',
                url: "/tiara-jewellery"
            }, {
                name: 'Hoops',
                url: "/hoops-jewellery"
            },
        ]
    },
};

//layer 3 named jewellery
export const Jewellery = {
    Jewellery: {
        Earrings: {
            name: "Earrings",
            url: "/earrings-jewellery",
            icon: 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHg9IjBweCIgeT0iMHB4Igp3aWR0aD0iMjUiIGhlaWdodD0iMjUiCnZpZXdCb3g9IjAgMCAxNzIgMTcyIgpzdHlsZT0iIGZpbGw6IzAwMDAwMDsiPjxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0ibm9uemVybyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIHN0cm9rZS1saW5lY2FwPSJidXR0IiBzdHJva2UtbGluZWpvaW49Im1pdGVyIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHN0cm9rZS1kYXNoYXJyYXk9IiIgc3Ryb2tlLWRhc2hvZmZzZXQ9IjAiIGZvbnQtZmFtaWx5PSJub25lIiBmb250LXdlaWdodD0ibm9uZSIgZm9udC1zaXplPSJub25lIiB0ZXh0LWFuY2hvcj0ibm9uZSIgc3R5bGU9Im1peC1ibGVuZC1tb2RlOiBub3JtYWwiPjxwYXRoIGQ9Ik0wLDE3MnYtMTcyaDE3MnYxNzJ6IiBmaWxsPSJub25lIj48L3BhdGg+PGcgZmlsbD0iI2JmYmZiZiI+PHBhdGggZD0iTTg2LDE3LjJjLTM3Ljk5NDgsMCAtNjguOCwzMC44MDUyIC02OC44LDY4LjhjMCwzNy45OTQ4IDMwLjgwNTIsNjguOCA2OC44LDY4LjhjMzcuOTk0OCwwIDY4LjgsLTMwLjgwNTIgNjguOCwtNjguOGMwLC0zNy45OTQ4IC0zMC44MDUyLC02OC44IC02OC44LC02OC44ek0xMjAuNCw5MS43MzMzM2gtMjguNjY2Njd2MjguNjY2NjdjMCwzLjE3MDUzIC0yLjU2ODUzLDUuNzMzMzMgLTUuNzMzMzMsNS43MzMzM2MtMy4xNjQ4LDAgLTUuNzMzMzMsLTIuNTYyOCAtNS43MzMzMywtNS43MzMzM3YtMjguNjY2NjdoLTI4LjY2NjY3Yy0zLjE2NDgsMCAtNS43MzMzMywtMi41NjI4IC01LjczMzMzLC01LjczMzMzYzAsLTMuMTcwNTMgMi41Njg1MywtNS43MzMzMyA1LjczMzMzLC01LjczMzMzaDI4LjY2NjY3di0yOC42NjY2N2MwLC0zLjE3MDUzIDIuNTY4NTMsLTUuNzMzMzMgNS43MzMzMywtNS43MzMzM2MzLjE2NDgsMCA1LjczMzMzLDIuNTYyOCA1LjczMzMzLDUuNzMzMzN2MjguNjY2NjdoMjguNjY2NjdjMy4xNjQ4LDAgNS43MzMzMywyLjU2MjggNS43MzMzMyw1LjczMzMzYzAsMy4xNzA1MyAtMi41Njg1Myw1LjczMzMzIC01LjczMzMzLDUuNzMzMzN6Ij48L3BhdGg+PC9nPjwvZz48L3N2Zz4=',
        },
        Pendants: {
            name: "Pendants",
            url: "/pendants-jewellery",
            icon: ""
        },
        Rings: {
            name: "Rings",
            url: "/rings-jewellery",
            icon: ""
        },
        NosePins: {
            name: "Nose Pins",
            url: "/nose+pin+online-jewellery",
            icon: ""
        },
        BanglesBracelets: {
            name: "Bangles & Bracelets",
            url: "/bangles-jewellery",
            icon: ""
        },
        ByPrice: {
            name: "By Price",
            url: "/#/",
            icon: ""
        },
        ByCollections: {
            name: "By Collections",
            url: "/pages/collections",
            icon: ""
        },
        MyMaterials: {
            name: "My Materials",
            url: "/#/",
            icon: ""
        },

    },
    Silver: {
        SilverJewellery: {
            name: "Silver Jewellery",
            url: "stylori.net/silver-jewellery",
            icon: ""
        }
    },
    GoldCoins: {
        Bydesign: {
            name: 'By design',
            url: "/#/",
            icon: ""
        },
        ByWeight: {
            name: 'By Weight',
            url: "/#/",
            icon: ""
        },
        ByPurity: {
            name: 'By Purity',
            url: "/#/",
            icon: ""
        }
    },
    Gifts: {
        Gifts: {
            name: 'Gifts',
            url: "/#/",
            icon: ""
        },
        ByPrice: {
            name: 'By Price',
            url: "/#/",
            icon: ""
        }
    },
    Collections: {
        ByCollections: {
            name: 'By Collections',
            url: "/#/",
            icon: ""
        },
        ByTheme: {
            name: 'By Theme',
            url: "/#/",
            icon: ""
        }
    },
    // "My Account": {
    //     Login: {
    //         name: 'Login',
    //         url: "/#/",
    //         icon: ""
    //     },
    //     Register: {
    //         name: 'Register',
    //         url: "/#/",
    //         icon: ""
    //     }
    // },
}


//desktop views
export const menuListHeader =
    [
        { title: 'JEWELLERY', url: "/jewellery" },
        { title: 'SILVER', url: "/stylorisilver" },
        { title: 'GOLD COINS', url: "/goldcoins" },
        { title: 'COLLECTIONS', url: "/collections" },
        { title: 'READY TO SHIP', url: "/jewellery-shipping+in+1+day" },
        { title: 'STORIES', url: "/stories" }
    ];


// List header hover
export const menuLists = {
    'JEWELLERY': {
        //----SUBLAYER1 ------
        'menuOne':
            [
                {
                    value: 'earrings', title: 'Earrings', url: '/earrings-jewellery',
                    imgContainer: {

                        //-----------LAYER 3--------
                        "imageContainer": [
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "Studs",
                                "url": "/studs-earrings-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Drops",
                                "url": "/drops-earrings-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE1025/SE1025-1Y.jpg",
                                "content": "Earcuffs",
                                "url": "/ear+cuffs-earrings-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Huggies",
                                "url": "/huggies-earrings-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "Jhumkas",
                                "url": "/jhumkas+online-earrings-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Ear Jacket",
                                "url": "/ear+jacket-earrings-jewellery",
                            },
                        ]
                    }
                },
                {
                    value: 'pendants', title: 'Pendants', url: '/pendants-jewellery',
                    imgContainer: {

                        "imageContainer": [
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "Classic",
                                "url": "/classic-pendants-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Casual",
                                "url": "/casual-pendants-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE1025/SE1025-1Y.jpg",
                                "content": "Fashion",
                                "url": "/fashion-pendants-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Religious",
                                "url": "/religious-pendants-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "Tanmaniya",
                                "url": "/tanmaniya-pendants-jewellery",
                            },
                        ]
                    }
                },
                {
                    value: 'rings', title: 'Rings', url: '/rings-jewellery',
                    imgContainer: {

                        "imageContainer": [
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "Classic",
                                "url": "/classic-rings-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Casual",
                                "url": "/casual-rings-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE1025/SE1025-1Y.jpg",
                                "content": "Cocktail",
                                "url": "/cocktail-rings-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Engagement",
                                "url": "/rings-jewellery-for+engagement",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "Fashion",
                                "url": "/fashion-rings-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Men's Ring",
                                "url": "/rings-jewellery-for+men",
                            },
                        ]
                    }
                },
                {
                    value: 'nosepins', title: 'Nose pins', url: '/nose+pin+online-jewellery',
                    imgContainer: {

                        "imageContainer": [
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "Classic",
                                "url": "/classic-nose+pin+online-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Fashion",
                                "url": "/fashion-nose+pin+online-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE1025/SE1025-1Y.jpg",
                                "content": "Stud Nose Pin",
                                "url": "/nose+studs+online-jewellery",
                            },
                        ]
                    }
                },
                {
                    value: 'banglesbracelets', title: 'Bangles & Bracelets', url: '/bangles-jewellery',
                    imgContainer: {

                        "imageContainer": [
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "Bangles",
                                "url": "/bangles-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Bracelets",
                                "url": "/bracelets-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE1025/SE1025-1Y.jpg",
                                "content": "Oval Bracelets",
                                "url": "/oval+bracelets-jewellery",
                            },
                        ]
                    }
                },
            ],
        //----SUBLAYER2 ------
        'menuTwo': [{
            value: 'Price', title: 'By Price', url: '#Price',
            imgContainer: {
                //-----------LAYER 3--------
                "onlyText": [
                    {
                        "content": "under 5000",
                        "url": "/jewellery?startprice=1000&endprice=5000"
                    },
                    {
                        "content": "5000-10000",
                        "url": "/jewellery?startprice=5000&endprice=10000"
                    },
                    {
                        "content": "10000-20000",
                        "url": "/jewellery?startprice=10000&endprice=20000"
                    },
                    {
                        "content": "above 20000",
                        "url": "/jewellery?startprice=20000&endprice=500000"
                    },
                ],

            }
        },
        {
            value: 'Collection', title: 'By Collection', url: '/collections',
            imgContainer: {
                "imageContainer": [
                    {
                        "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                        "content": "Carve",
                        "url": "/jewellery-from+the+carve+collection?sort=latest",
                    },
                    {
                        "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                        "content": "Blush",
                        "url": "/jewellery-blush",
                    },
                    {
                        "img": "https://assets-cdn.stylori.com/276x276/images/product/SE1025/SE1025-1Y.jpg",
                        "content": "Molecute",
                        "url": "/jewellery-molecute+collection",
                    },
                    {
                        "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                        "content": "Gemstone",
                        "url": "/gemstone-jewellery?sort=latest",
                    },
                    {
                        "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                        "content": "Mistletoe",
                        "url": "/jewellery-from+mistletoe+collection",
                    },
                    {
                        "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                        "content": "The Renaissance",
                        "url": "/jewellery-from+the+renaissance+collection",
                    },
                    {
                        "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                        "content": "Monsoon",
                        "url": "/jewellery-from+monsoon+collection",
                    },
                    {
                        "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                        "content": "Summer",
                        "url": "/jewellery-from+the+summer+collection",
                    },
                ]
            }
        },
        {
            value: 'Material', title: 'By Material', url: '#',
            imgContainer: {
                "onlyText": [{
                    "content": "Diamond",
                    "url": "/diamond-jewellery"
                },
                {
                    "content": "Gemstone",
                    "url": "/gemstone-jewellery"
                },
                {
                    "content": "Solitaire",
                    "url": "/solitaire-jewellery"
                },
                ],
            }
        },
        ]
    },
    'SILVER': {
        'menuOne':
            [
                {
                    value: 'silverjewellery',
                    title: 'Silver Jewellery',
                    url: '/silver-jewellery',
                    imgContainer: {

                        "imageContainer": [
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "Earrings",
                                "url": "/silver-earrings-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Rings",
                                "url": "/silver-rings-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE1025/SE1025-1Y.jpg",
                                "content": "Pendants",
                                "url": "/silver-pendants-jewellery",
                            },
                        ]
                    }
                }]
    },
    'GOLDCOINS': {
        'menuOne':
            [
                {
                    value: 'bydesign',
                    title: 'By design',
                    url: '#Bydesign',
                    imgContainer: {

                        "imageContainer": [
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "Plain",
                                "url": "/plain-goldcoins",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Lakshmi",
                                "url": "/lakshmi-goldcoins",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE1025/SE1025-1Y.jpg",
                                "content": "Jesus",
                                "url": "/jesus-goldcoins",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Ganesh ",
                                "url": "/ganesha-goldcoins",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "Balaji",
                                "url": "/balaji-goldcoins",
                            },
                        ]
                    }
                },
                {
                    value: 'byweight', title: 'By Weight', url: '#',
                    imgContainer: {

                        "imageContainer": [
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "1 Gram",
                                "url": "/1gm-goldcoins",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "4 Grams",
                                "url": "/4gms-goldcoins",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE1025/SE1025-1Y.jpg",
                                "content": "8 Grams",
                                "url": "/8gms-goldcoins",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "10 Grams",
                                "url": "/10gms-goldcoins",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "20 Grams",
                                "url": "/20gms-goldcoins",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "50 Grams",
                                "url": "/50gms-goldcoins",
                            },
                        ]
                    }
                },
                {
                    value: 'bypurity', title: 'By Purity', url: '#',
                    imgContainer: {

                        "imageContainer": [
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "24 KT",
                                "url": "/24kt-goldcoins",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "22 KT",
                                "url": "/22kt-goldcoins",
                            },
                        ]
                    }
                },
            ],
    },
    'COLLECTIONS': {
        'menuOne':
            [
                {
                    value: 'bycollections',
                    title: 'By Collections',
                    url: '#',
                    imgContainer: {

                        "imageContainer": [
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "Halo",
                                "url": "/jewellery-from+halo+collection",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Daisy Days",
                                "url": "/jewellery-from+daisy+days+collection",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE1025/SE1025-1Y.jpg",
                                "content": "Monsoon",
                                "url": "/jewellery-from+monsoon+collection",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Mango",
                                "url": "/jewellery-from+the+summer+collection",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "Blush",
                                "url": "/jewellery-blush",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Molecute",
                                "url": "/jewellery-molecute+collection",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Gemstone",
                                "url": "/gemstone-jewellery?sort=latest",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Butterfly",
                                "url": "/jewellery-butterfly?sort=latest",
                            },
                        ]
                    }
                },
                {
                    value: 'bytheme', title: 'By Theme', url: '#',
                    imgContainer: {

                        "imageContainer": [
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "Carve",
                                "url": "/jewellery-from+the+carve+collection?sort=latest",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Cluster",
                                "url": "/cluster-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE1025/SE1025-1Y.jpg",
                                "content": "Droplets",
                                "url": "/droplets-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Waves",
                                "url": "/waves-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/276x276/images/product/SE0889/SE0889-1Y.jpg",
                                "content": "Floral",
                                "url": "/waves-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Hearts",
                                "url": "/floral-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Tiara",
                                "url": "/tiara-jewellery",
                            },
                            {
                                "img": "https://assets-cdn.stylori.com/296x296/images/product/SR0201/SR0201-1YW.jpg",
                                "content": "Hoops",
                                "url": "/hoops-jewellery",
                            },
                        ]
                    }
                },
            ]
    },
}

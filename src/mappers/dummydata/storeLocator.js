export const storeLocatorData={

    "carouselTop": {
        "setting": {
            dots: false,
            infinite: false,
            autoplay: false,
            speed: 1000,
            fade: false,
            arrows: false,
            arrowsImg: false,
            dotsClass: "slickdev",
            accessibility: false,
            centerMode: false,
            focusOnSelect: false,
            pauseOnHover: false,
            pauseOnDotsHover: false,
            pauseOnFocus: false,
            swipe: false
        },
        "data": [
            {
                img: "https://alpha-assets.stylori.com/images/static/stylori_faq.png",
            },

        ]
    },
    data:[
        {
            img:"https://storage.googleapis.com/media.nacjewellers.com/resources/store-locator/Mylapore.jpg",
            href:"",
            title:"Mylapore",
            para:"Plot No. 58, North Mada Street, Mylapore,<br>Chennai, Tamil Nadu 600004",
            button:"View Store Details",
            key:"ChIJD2b_DdNnUjoRu9gobrKLB8I"
        },
        {
            img:"https://storage.googleapis.com/media.nacjewellers.com/resources/store-locator/velachery.jpg",
            href:"",
            title:"Velachery",
            para:"No: 465/5, Velachery Bypass Road, Rajalakshmi Nagar, Velachery,<br>Chennai, Tamil Nadu 600042",
            button:"View Store Details",
            key:"ChIJR-r-zY5dUjoRKhDlVtRh39I"
        },
        {
            img:"https://storage.googleapis.com/media.nacjewellers.com/resources/store-locator/Perambur.jpg",
            href:"",
            title:"Perambur",
            para:"No: 39, 41 Paper Mills Road, Siruvallur, Perambur,<br>Chennai, Tamil Nadu 600011",
            button:"View Store Details",
            key:"ChIJPyqXbLFlUjoRlijMXUh0R84"
            
        },
        {
            img:"https://storage.googleapis.com/media.nacjewellers.com/resources/store-locator/KANCHIPURAM%20SHOWROOM.jpg",
            href:"",
            title:"Kanchipuram",
            para:"No: 30, Kamarajar Salai, Kanchipuram,<br>Tamil Nadu 631502",
            button:"View Store Details",
            key:"ChIJiZb4tVfCUjoRWHb4JCGQyKY"

        },
        {
            img:"https://storage.googleapis.com/media.nacjewellers.com/resources/store-locator/Tiruvallur.jpg",
            href:"",
            title:"Tiruvallur",
            para:"Plot no.216/2,216/3, S.no.196/2 part, Kakkalur village, TNHB Road, Ma. Po. Si. Nagar,<br>Tiruvallur, Tamil Nadu 602001",
            button:"View Store Details",
            key:"ChIJdUFnBRiQUjoR7wz1VkiWT3w"
,
        },
        {
            img:"https://storage.googleapis.com/media.nacjewellers.com/resources/store-locator/Anna%20Nagar.jpg",
            href:"",
            title:"Anna Nagar",
            para:"New No 73, AG Block, 4th Avenue, 7th Main Road, Shanthi Colony,River View Colony,<br>Chennai, Tamil Nadu 600040",
            button:"View Store Details",
            key:"ChIJq_WsMidkUjoRH7FAOtdaXis"

        },
        {
            img:"https://storage.googleapis.com/media.nacjewellers.com/resources/store-locator/Vijayawada.jpg",
            href:"",
            title:"Vijayawada",
            para:"41, 40-1-0, Mahatma Gandhi Rd, Near petrol Bunk, Labbipet,<br>Vijayawada, Andhra Pradesh 520010",
            button:"View Store Details",
            key:"ChIJlZ2oZrr6NToRG4kJpSN1uf8",

        },
        {
            img:"https://storage.googleapis.com/media.nacjewellers.com/resources/store-locator/OLDWASHERMENPET.jpg",
            href:"",
            title:"Old Washermanpet",
            para:"456, Thiruvottiyur High Rd, Korukkupet, Old Washermanpet,<br>Chennai, Tamil Nadu 600021",
            button:"View Store Details",
            key:"ChIJPSl-5WNvUjoRhtsRqpmTVTc"

        },
        {
            img:"https://storage.googleapis.com/media.nacjewellers.com/resources/store-locator/t.nagar.jpg",
            href:"",
            title:"T. Nagar",
            para:"20, North Usman Road, Thiyagaraya Nagar,<br>Chennai, Tamil Nadu 600017",
            button:"View Store Details",
            key:"ChIJ423xiS1mUjoR_LOZTgQBMMM"

        },
    ]

}
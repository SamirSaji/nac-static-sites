export const
  diamondsData = {

    "carouselTop": {
      "setting": {
        dots: false,
        infinite: true,
        autoplay: true,
        speed: 1000,
        fade: false,
        arrows: false,
        arrowsImg: true,
        dotsClass: "slickdev",
        accessibility: true,
        centerMode: false,
        focusOnSelect: false,
        pauseOnHover: false,
        pauseOnDotsHover: false,
        pauseOnFocus: true,
        swipe: false,
      },
      "data": [
        {
          img: "https://storage.googleapis.com/media.nacjewellers.com/resources/user_media/2019/Youngones_NACWEB_Banner_1189X500.jpg",
          navigateUrl: "/gemstone-jewellery?sort=latest"
        },
        {
          img: "https://storage.googleapis.com/media.nacjewellers.com/resources/user_media/2019/Muhurtham_NACWEB_Banner_1189X500.jpg",
          navigateUrl: "/jewellery-shipping+in+1+day?sort=featured"
        },
        {
          img: "https://storage.googleapis.com/media.nacjewellers.com/resources/user_media/2019/J%20Rewind_new2019.jpg",
          navigateUrl: "/rings-jewellery"
        },

      ]
    },

    data: {
      heading: "Loose Diamonds",
      para: "Set as the centrepiece and surrounded by strutting burnished peacocks, this pendant is a worthy heirloom to possess. The Thala Billai pendant comes with a matching pair of peacock shaped earrings."

    },
    ImageData: [
      {
        image: "image1",
        name: "Round",

      },
      {
        image: "image2",
        name: "Princess",

      },
      {
        image: "image3",
        name: "Ascher",

      },
      {
        image: "image4",
        name: "Emerald",
      },
      {
        image: "image5",
        name: "Radiant",
      },
      {
        image: "image6",
        name: "Heart",
      },
      {
        image: "image7",
        name: "Marquise",
      },
      {
        image: "image8",
        name: "Pear",
      },
      {
        image: "image9",
        name: "Oval",
      },
    ],

    cut: [
      {
        name: "Fair Cut"
      },
      {
        name: "Good Cut"

      },
      {
        name: " Very Good Cut"
      },
      {
        name: "Excellent"
      },
      {
        name: "None"
      },
    ],
    clarity: [
      {
        name: "SL2",
      },
      {
        name: "SL1",
      },
      {
        name: "VS2",
      },

      { name: "VS1", },

      { name: "VVS1", },

      { name: "VVS2", },

      { name: "IF", },
      { name: "FL", },
      { name: "L1", },
      { name: "None" },

    ],
    colour: [
      {
        name: "D",
      },
      {
        name: "E",
      },
      {
        name: "F",
      },

      { name: "G", },

      { name: "H", },

      { name: "I", },

      { name: "J", },
      { name: "K", },
      { name: "L", },
      { name: "M" },
      { name: "N" },
      { name: "O-Z" },


    ],
    certifiy: [
      {
        name: "GIA",
      },
      {
        name: "IGI",
      },
      {
        name: "NON",
      },
      {
        name: "HRD",
      },
      {
        name: "IIDGR",
      },


    ],

  }
import { Hidden } from "@material-ui/core";

export const homeNac = {
  carouselTop: {
    setting: {
      dots: false,
      infinite: true,
      autoplay: true,
      speed: 1000,
      fade: false,
      arrows: false,
      arrowsImg: true,
      dotsClass: "slickdev",
      accessibility: true,
      centerMode: false,
      focusOnSelect: false,
      pauseOnHover: false,
      pauseOnDotsHover: false,
      pauseOnFocus: true,
      swipe: false,
    },
    data: [
      {
        img:
          "https://storage.googleapis.com/media.nacjewellers.com/resources/user_media/2019/Youngones_NACWEB_Banner_1189X500.jpg",
        navigateUrl: "/gemstone-jewellery?sort=latest",
      },
      {
        img:
          "https://storage.googleapis.com/media.nacjewellers.com/resources/user_media/2019/Muhurtham_NACWEB_Banner_1189X500.jpg",
        navigateUrl: "/jewellery-shipping+in+1+day?sort=featured",
      },
      {
        img:
          "https://storage.googleapis.com/media.nacjewellers.com/resources/user_media/2019/J%20Rewind_new2019.jpg",
        navigateUrl: "/rings-jewellery",
      },
    ],
  },
};

export const storyData = {
  data: {
    heading: "The NAC Story",
    para:
      "In 1973, Shri N. Anjaneyalu Chetty started a jewellery shop in Mylapore hoping to find loyal patrons. What began as a small store was soon a flourishing business and NAC became a household name in Mylapore! NAC’s penchant for heritage jewels helped to carve a niche in a city, where culture, heritage and history are cherished, even to this day. Today, NAC has expanded its arms and has found many place in and around Chennai and an exclusive showroom at Vijaywada.  From a humble beginning to seven plush three-storey showrooms, the journey has been eventful with many accolades and commendations for our commitment to the ancient art of jewellery making.",
    store: "Locate a store",
  },
};

export const HomeCardData = {
  data: [
    {
      img:
        "https://storage.googleapis.com/media.nacjewellers.com/resources/collection_files/rewind/All%20Collection%20Mobile%2001.jpg",
      heading: "Young One",
      para:
        "Adorn your kids with trendy and fashionable jewellery with exhaustive themes and options. Gift your child all the glitter and sparkle in the world and match it up with their adorable smiles. Specially designed with trends that appeal to children, our",
      anchor: "view collection",
    },
    {
      img:
        " https://storage.googleapis.com/media.nacjewellers.com/resources/collection_files/breezee/All%20Collection%20Mobile%2002.jpg",
      heading: "Young One",
      para:
        "Adorn your kids with trendy and fashionable jewellery with exhaustive themes and options. Gift your child all the glitter and sparkle in the world and match it up with their adorable smiles. Specially designed with trends that appeal to children, our",
      anchor: "view collection",
    },
    {
      img:
        "https://storage.googleapis.com/media.nacjewellers.com/resources/collection_files/rewind/All%20Collection%20Mobile%2001.jpg",
      heading: "Young One",
      para:
        "Adorn your kids with trendy and fashionable jewellery with exhaustive themes and options. Gift your child all the glitter and sparkle in the world and match it up with their adorable smiles. Specially designed with trends that appeal to children, our",
      anchor: "view collection",
    },
  ],
};

export const StaticImage = {
  setting: {
    dots: false,
    infinite: false,
    autoplay: false,
    speed: 1000,
    fade: false,
    arrows: false,
    arrowsImg: true,
    dotsClass: "slickdev",
    accessibility: true,
    centerMode: false,
    focusOnSelect: false,
    pauseOnHover: false,
    pauseOnDotsHover: false,
    pauseOnFocus: true,
    swipe: false,
  },

  data: [
    {
      img:
        "https://storage.googleapis.com/media.nacjewellers.com/resources/user_media/2020%20Banners/Bridal%20Clllection_NAC%20Jewellers.jpg",
    },
  ],
};

export const Testimonial = {
  setting: {
    dots: false,
    infinite: true,
    autoplay: true,
    speed: 1000,
    fade: false,
    arrows: false,
    arrowsImg: true,
    dotsClass: "slickdev",
    accessibility: true,
    centerMode: false,
    focusOnSelect: false,
    pauseOnHover: false,
    pauseOnDotsHover: false,
    pauseOnFocus: true,
    swipe: false,
  },

  data: [
    {
      img:
        "https://storage.googleapis.com/media.nacjewellers.com/resources/collection_files/rewind/All%20Collection%20Mobile%2001.jpg",
      heading: "Young One",
      para:
        "I thought that simple and subtle suited my personality until I entered NAC Jewellers to shop for my wedding. Every jewel that I picked up had made intricate details and delicate craftsmanship that is hard to find in any other store. The sheer variety...",
      anchor: "view collection",
      country: "India",
    },
    {
      img:
        " https://styloriimages.s3.ap-south-1.amazonaws.com/images/Static+Pages/Home+Page/banner5.jpg",
      heading: "Yofhfbgfgbfg",
      para:
        "I thought that simple and subtle suited my personality until I entered NAC Jewellers to shop for my wedding. Every jewel that I picked up had made intricate details and delicate craftsmanship that is hard to find in any other store. The sheer variety...",
      anchor: "view lijikjlkjkljlklkl;kl;k",
      country: "India",
    },
    {
      img:
        "https://storage.googleapis.com/media.nacjewellers.com/resources/user_profile/Pratyusha-Garlapati-Testimonial.jpg",

      heading: "Young One",
      para:
        "I thought that simple and subtle suited my personality until I entered NAC Jewellers to shop for my wedding. Every jewel that I picked up had made intricate details and delicate craftsmanship that is hard to find in any other store. The sheer variety...",
      anchor: "view collection",
      country: "India",
    },
    {
      img:
        " https://styloriimages.s3.ap-south-1.amazonaws.com/images/Static+Pages/Home+Page/banner5.jpg",
      heading: "Young One",
      para:
        "I thought that simple and subtle suited my personality until I entered NAC Jewellers to shop for my wedding. Every jewel that I picked up had made intricate details and delicate craftsmanship that is hard to find in any other store. The sheer variety...",
      anchor: "view collection",
      country: "India",
    },
    {
      img:
        " https://styloriimages.s3.ap-south-1.amazonaws.com/images/Static+Pages/Home+Page/banner5.jpg",
      heading: "Young One",
      para:
        "I thought that simple and subtle suited my personality until I entered NAC Jewellers to shop for my wedding. Every jewel that I picked up had made intricate details and delicate craftsmanship that is hard to find in any other store. The sheer variety...",
      anchor: "view collection",
      country: "India",
    },
    {
      img:
        "https://storage.googleapis.com/media.nacjewellers.com/resources/user_profile/Kani-Prabha-Testimonial.jpg",
      heading: "Young One",
      para:
        "I thought that simple and subtle suited my personality until I entered NAC Jewellers to shop for my wedding. Every jewel that I picked up had made intricate details and delicate craftsmanship that is hard to find in any other store. The sheer variety...",
      anchor: "view collection",
      country: "India",
    },
  ],
};
// export const Testimonial1 = {}
// export const Testimonial2 = {}
